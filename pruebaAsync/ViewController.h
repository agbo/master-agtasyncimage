//
//  ViewController.h
//  pruebaAsync
//
//  Created by Fernando Rodríguez Romero on 16/04/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AGTAsyncImage.h"

@interface ViewController : UIViewController<AGTAsyncImageDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
- (IBAction)flushThis:(id)sender;

- (IBAction)flushAll:(id)sender;

@end

