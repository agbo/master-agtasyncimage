//
//  ViewController.m
//  pruebaAsync
//
//  Created by Fernando Rodríguez Romero on 16/04/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "ViewController.h"


@interface ViewController ()
@property (nonatomic, strong) AGTAsyncImage *aImage;
@end

@implementation ViewController

- (IBAction)getImage:(id)sender {
    
    NSURL *hugeImage = [NSURL URLWithString:@"http://www.nasa.gov/sites/default/files/pia17679.jpg"];
    NSURL *smallImage = [NSURL URLWithString:@"https://cdn4.iconfinder.com/data/icons/black-icon-social-media/256/099350-reddit-logo.png"];
    NSURL *bigImage = [NSURL URLWithString:@"http://theworldwentaway.files.wordpress.com/2012/09/mariana-y-camila-davalos-08.jpg"];
    
    self.aImage = [[AGTAsyncImage alloc] initWithURL:bigImage
                                        defaultImage:[UIImage imageNamed:@"delavega-playa.jpg"]];
    self.imageView.image = self.aImage.image;
    self.aImage.delegate = self;
    

}
-(void)asyncImageDidChange:(AGTAsyncImage *)aImage{
    
    NSLog(@"https://www.youtube.com/watch?v=fq65OcyOv0Y");
    [UIView transitionWithView:self.view
                      duration:0.9f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        self.imageView.image = aImage.image;
                    } completion:nil];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)flushThis:(id)sender {
    
    [self.aImage flushLocalCache];
}

- (IBAction)flushAll:(id)sender {
    
    [AGTAsyncImage flushLocalCache];
    
}
@end
